nohupx: an enhanced nohup
=========================

More control on bash jobs detached from the current shell.

Install `nohups` by putting it somewhere in your `PATH`.

Quick start
------------

```bash
nohupx run sleep 100
nohupx run sleep 101
```

```bash
nohupx queue
```

```
[1] R 1601645 1601645 coslo sleep 101
[2] R 1601645 1601630 coslo sleep 100
```

Kill the jobs
```bash
nohupx kill 1 2
```

```
Killed 1601630
Killed 1601645
```

Add a few aliases for convenience
```bash
alias nrun="nohupx run"
alias nqueue="nohupx queue"
alias nkill="nohupx kill"
```
